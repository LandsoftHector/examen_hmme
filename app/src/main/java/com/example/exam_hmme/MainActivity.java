package com.example.exam_hmme;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.example.exam_hmme.databinding.ActivityMainBinding;
import com.example.exam_hmme.home.InicioActivity;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private int REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.idMovies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InicioActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, "Movies");
                startActivity(intent);
            }
        });

        binding.idLocalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InicioActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, "Location");
                startActivity(intent);
            }
        });

        binding.idUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, InicioActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, "Upload");
                startActivity(intent);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {
        super.onStart();
        verificaPermisos();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void verificaPermisos() {
        int permisoAlmacenamiento = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        //int permisoCamara = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if(permisoAlmacenamiento == PackageManager.PERMISSION_DENIED){
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
        }
    }

}