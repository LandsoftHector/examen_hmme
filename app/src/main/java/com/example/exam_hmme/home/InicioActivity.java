package com.example.exam_hmme.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import com.example.exam_hmme.R;
import com.example.exam_hmme.home.location.LocationFragment;
import com.example.exam_hmme.home.movie.MoviesFragment;
import com.example.exam_hmme.home.upload.UploadFragment;

public class InicioActivity extends AppCompatActivity {

    private FragmentTransaction transaction;
    private String namePantalla = null;
    private Fragment fragmenMovies;
    private Fragment fragmenUpload;
    private Fragment fragmentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        Intent intent = getIntent();
        try{
            if(intent.hasExtra(Intent.EXTRA_TEXT)) {
                namePantalla = (String) intent.getSerializableExtra(Intent.EXTRA_TEXT);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        fragmenMovies = new MoviesFragment();
        fragmenUpload = new UploadFragment();
        fragmentLocation = new LocationFragment();

        if(namePantalla != null){
            lanzarPantalla();
        }
    }

    private void lanzarPantalla() {

        try {
            transaction = getSupportFragmentManager().beginTransaction();
            if(namePantalla.equals(getString(R.string.pantalla_movies))){
                getSupportActionBar().setTitle(getString(R.string.pantalla_movies));
                transaction.replace(R.id.contenedor, fragmenMovies);
                transaction.commit();
            }
            if(namePantalla.equals(getString(R.string.pantalla_location))){
                getSupportActionBar().setTitle(getString(R.string.pantalla_location));
                transaction.replace(R.id.contenedor, fragmentLocation);
                transaction.commit();
            }
            if(namePantalla.equals(getString(R.string.pantalla_upload))){
                getSupportActionBar().setTitle(getString(R.string.pantalla_upload));
                transaction.replace(R.id.contenedor, fragmenUpload);
                transaction.commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}