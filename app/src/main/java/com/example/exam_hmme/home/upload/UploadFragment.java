package com.example.exam_hmme.home.upload;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.exam_hmme.R;
import com.example.exam_hmme.data.adapter.AdapterImagenesUpload;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class UploadFragment extends Fragment {

    int TOMAR_FOTO = 100;
    int SELEC_IMAGEN = 200;
    int SELEC_UNA_IMAGEN = 300;
    private static final int RESULT_OK =  -1;
    private MaterialButton btnTomarFoto, btnSeleccionarImagen,subirImagen,selecionarUnaImagen;
    private List<Uri> uriList = new ArrayList<>();
    private Uri imagenUri;
    private AdapterImagenesUpload adapterImagenesUpload;
    private RecyclerView  recyclerViewImg;
    private ImageView imageView;
    private String path = "";
    private ProgressDialog progressDialog;
    private String rutaImagen = "";
    private DatabaseReference myRef;
    private Intent data_2;
    boolean multipleUpload = false;
    boolean imgCamara = false;
    boolean imgGaleria = false;
    TextView textUploadSucces, textInicio;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload, container, false);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef= database.getReference("user1");

        btnSeleccionarImagen = view.findViewById(R.id.abrirGaleria);
        btnTomarFoto = view.findViewById(R.id.camara);
        recyclerViewImg = view.findViewById(R.id.reciclerImagenes);
        imageView = view.findViewById(R.id.imgCamara);
        progressDialog = new ProgressDialog(getContext());
        subirImagen = view.findViewById(R.id.subirImagen);
        selecionarUnaImagen = view.findViewById(R.id.abrirGaleria_1);
        textUploadSucces  = view.findViewById(R.id.textUploadSucess);
        textInicio = view.findViewById(R.id.texInicio);

        recyclerViewImg.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
        textUploadSucces.setVisibility(View.GONE);
        textInicio.setVisibility(View.VISIBLE);

        
        btnSeleccionarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarImagenes();
            }
        });

        selecionarUnaImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionarUnaImagen();
            }
        });

        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirCamara();
            }
        });

        subirImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!multipleUpload && !imgGaleria && !imgCamara){
                    Toast.makeText(getContext(), "Seleccione Imagen o Tome una fotografia.", Toast.LENGTH_SHORT).show();
                    return;
                }
                initDialog("", "Cargando Imagen");
                cargarImagen(path);
            }
        });

        return view;
    }

    private void seleccionarUnaImagen() {
        Intent galeria = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(galeria, SELEC_UNA_IMAGEN);
    }

    private void cargarImagen(String path) {
        try {

            //Uri total_1 = data_2.getData();
            if(multipleUpload){
                int total = data_2.getClipData().getItemCount();
                for (int i = 0; i<total; i++){
                    Uri imageUri = data_2.getClipData().getItemAt(i).getUri();
                    //String imagename = getFileName(imageUri);
                    StorageReference Folder = FirebaseStorage.getInstance().getReference().child("User");
                    StorageReference mRef = Folder.child("file"+ imageUri.getLastPathSegment());

                    mRef.putFile(imageUri).addOnSuccessListener(taskSnapshot -> mRef.getDownloadUrl().addOnSuccessListener(uri -> {

                        HashMap<String,String> hashMap = new HashMap<>();
                        hashMap.put("link", String.valueOf(uri));
                        myRef.setValue(hashMap);
                        //ivFoto.setImageURI(Uri.parse(String.valueOf(R.drawable.ic_upload_file)));
                        recyclerViewImg.setVisibility(View.GONE);
                        imageView.setVisibility(View.GONE);
                        textUploadSucces.setVisibility(View.VISIBLE);
                        textInicio.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        uriList = new ArrayList<>();
                        multipleUpload = false;
                    }));
                }
            }
            if(imgGaleria){
                Uri file;
                file = Uri.fromFile(new File(rutaImagen));
                StorageReference Folder = FirebaseStorage.getInstance().getReference().child("User");
                StorageReference mRef = Folder.child("file"+ file.getLastPathSegment());

                mRef.putFile(file).addOnSuccessListener(taskSnapshot -> mRef.getDownloadUrl().addOnSuccessListener(uri -> {

                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("link", String.valueOf(uri));
                    myRef.setValue(hashMap);
                    //ivFoto.setImageURI(Uri.parse(String.valueOf(R.drawable.ic_upload_file)));
                    progressDialog.dismiss();
                    recyclerViewImg.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    textUploadSucces.setVisibility(View.VISIBLE);
                    textInicio.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Imagen cargada correctamente", Toast.LENGTH_SHORT).show();
                    Log.d("Mensaje", "Se subió correctamente");
                    imgGaleria =false;
                }));
            }

            if(imgCamara){
                Uri file;
                file = Uri.fromFile(new File(path));
                StorageReference Folder = FirebaseStorage.getInstance().getReference().child("User");
                StorageReference mRef = Folder.child("file"+ file.getLastPathSegment());
                mRef.putFile(file).addOnSuccessListener(taskSnapshot -> mRef.getDownloadUrl().addOnSuccessListener(uri -> {

                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("link", String.valueOf(uri));
                    myRef.setValue(hashMap);
                    //ivFoto.setImageURI(Uri.parse(String.valueOf(R.drawable.ic_upload_file)));
                    progressDialog.dismiss();
                    recyclerViewImg.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    textUploadSucces.setVisibility(View.VISIBLE);
                    textInicio.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Imagen cargada correctamente", Toast.LENGTH_SHORT).show();
                    Log.d("Mensaje", "Se subió correctamente");
                    imgCamara =false;
                }));

            }
        }catch (Exception e){
            e.printStackTrace();
            progressDialog.dismiss();
        }
    }

    private void seleccionarImagenes() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, SELEC_IMAGEN);
    }

    private void abrirCamara()  {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            File imagenArchivo = null;
            try {
                imagenArchivo = crearImagen();
                if(imagenArchivo != null){
                    Uri fotoUri = FileProvider.getUriForFile(getContext(), "com.example.exam_hmme.fileprovider", imagenArchivo);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fotoUri);
                    startActivityForResult(intent, TOMAR_FOTO);
                }

            }catch (IOException e){
                Log.e("ERROR", e.toString());
            }
        }
    }

    private File crearImagen() throws IOException {
        String nombreImagen=(System.currentTimeMillis() / 1000)+"";
        File directorio = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File  imagen = File.createTempFile(nombreImagen, ".jpg", directorio);
        path = imagen.getAbsolutePath();
        //rutaImagen =null;
        return imagen;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if(resultCode == RESULT_OK && requestCode == SELEC_IMAGEN) {
                ClipData clipData = data.getClipData();
                data_2 = data;
                //Si seleccione una sola Imagen
                if(clipData==null){
                    Toast.makeText(getContext(), " Seleccione mas de una imagen ", Toast.LENGTH_SHORT).show();
                    return;
                }
                // Si selecciono mas de una iamgen.
                else{
                    multipleUpload = true;
                    imgCamara =false;
                    imgGaleria = false;
                    for (int i = 0; i<clipData.getItemCount(); i++){
                        uriList.add(clipData.getItemAt(i).getUri());
                    }
                }
                recyclerViewImg.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
                textUploadSucces.setVisibility(View.GONE);
                textInicio.setVisibility(View.GONE);
                adapterImagenesUpload = new AdapterImagenesUpload(getContext(), uriList);
                recyclerViewImg.setLayoutManager(new GridLayoutManager(getContext(), 2));
                recyclerViewImg.setAdapter(adapterImagenesUpload);

            }
            if (requestCode == TOMAR_FOTO && resultCode == RESULT_OK) {
                multipleUpload = false;
                imgCamara =true;
                imgGaleria = false;
                recyclerViewImg.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                textUploadSucces.setVisibility(View.GONE);
                textInicio.setVisibility(View.GONE);
                MediaScannerConnection.scanFile(getContext(), new String[]{path}, null, (s, uri) -> {
                });
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                imageView.setImageBitmap(bitmap);
            }

            if (requestCode == SELEC_UNA_IMAGEN && resultCode == RESULT_OK) {
                multipleUpload = false;
                imgCamara =false;
                imgGaleria = true;
                recyclerViewImg.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                textUploadSucces.setVisibility(View.GONE);
                textInicio.setVisibility(View.GONE);
                imagenUri = data.getData();
                Uri selectedImageUri = data.getData();
                rutaImagen = getRealPathFromURI(selectedImageUri);
                imageView.setImageURI(imagenUri);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void initDialog(String title, String message) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMax(100);
        progressDialog.setMessage(message);
        progressDialog.setTitle(title);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
    }
}