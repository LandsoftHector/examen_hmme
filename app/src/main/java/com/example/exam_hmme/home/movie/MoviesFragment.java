package com.example.exam_hmme.home.movie;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.exam_hmme.R;
import com.example.exam_hmme.data.adapter.AdapterMovies;
import com.example.exam_hmme.data.model.MovieDetails;
import com.example.exam_hmme.data.model.Movies;
import com.example.exam_hmme.databinding.FragmentMoviesBinding;
import com.example.exam_hmme.utils.Constantes;

public class MoviesFragment extends Fragment implements  AdapterMovies.OnItemPosicionClick{

    FragmentMoviesBinding fragmentMoviesBinding;
    MoviesViewModel moviesViewModel;
    AdapterMovies adapterMovies;

    public MoviesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentMoviesBinding =  DataBindingUtil.inflate(inflater, R.layout.fragment_movies, container, false);
        View root = fragmentMoviesBinding.getRoot();
        moviesViewModel = new ViewModelProvider(this, new MoviesViewModelFactory()).get(MoviesViewModel.class);

        fragmentMoviesBinding.recyclerViewMovies.setVisibility(View.GONE);
        fragmentMoviesBinding.progressMovies.setVisibility(View.VISIBLE);

        moviesViewModel.getMoviesDiscover(Constantes.apiKey);


        moviesViewModel.getMoviesMutableLiveData().observe(getViewLifecycleOwner(), movies -> {
            if(movies==null){
                return;
            }
            if(movies != null){
                fragmentMoviesBinding.recyclerViewMovies.setVisibility(View.VISIBLE);
                fragmentMoviesBinding.progressMovies.setVisibility(View.GONE);
                adapterMovies = new AdapterMovies(getContext(),movies,this);
                fragmentMoviesBinding.recyclerViewMovies.setLayoutManager(new GridLayoutManager(getContext(), 2));
                fragmentMoviesBinding.recyclerViewMovies.setAdapter(adapterMovies);
                saveDatos(movies);
            }
        });
        return root;
    }

    private void saveDatos(Movies movies) {


    }

    @Override
    public void onClick(int posicion) {
        MovieDetails movieDetails = adapterMovies.getposicionmovieDetails(posicion);
        Bundle bundle = new Bundle();
        bundle.putParcelable("MovieDetails", movieDetails);
        DetailsBottomFragment addPhotoBottomDialogFragment =
                DetailsBottomFragment.newInstance(getActivity().getApplication());
        addPhotoBottomDialogFragment.setArguments(bundle);
        addPhotoBottomDialogFragment.show(getActivity().getSupportFragmentManager(),
                DetailsBottomFragment.TAG);
    }
}