package com.example.exam_hmme.home.location;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.exam_hmme.data.model.Usuarios;
import com.example.exam_hmme.data.repository.LocationRepository;

import java.util.List;

public class LocationViewModel extends AndroidViewModel {

    private final LocationRepository locationRepository;
    MutableLiveData<List<Usuarios>> mutableLiveData = new MutableLiveData<>();

    public LocationViewModel(@NonNull Application application, LocationRepository locationRepository) {
        super(application);
        this.locationRepository = locationRepository;
        mutableLiveData = locationRepository.getMutableLiveData();

    }

    public  void getDatos(){
        locationRepository.getDatosUsuario();
    }

    public MutableLiveData<List<Usuarios>> getMutableLiveData() {
        return mutableLiveData;
    }

}
