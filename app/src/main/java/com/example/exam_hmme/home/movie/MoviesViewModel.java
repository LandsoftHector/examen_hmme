package com.example.exam_hmme.home.movie;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.exam_hmme.data.model.Movies;
import com.example.exam_hmme.data.repository.MoviesRepository;

public class MoviesViewModel extends ViewModel {

    private MoviesRepository moviesRepository;
    private MutableLiveData<Movies> moviesMutableLiveData = new MutableLiveData<>();

    public MoviesViewModel(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
        moviesMutableLiveData = moviesRepository.getMoviesMutableLiveData();


    }

    public void getMoviesDiscover(String apiKey){
        moviesRepository.getMoviesDiscover(apiKey);
    }

    public MutableLiveData<Movies> getMoviesMutableLiveData() {
        return moviesMutableLiveData;
    }
}
