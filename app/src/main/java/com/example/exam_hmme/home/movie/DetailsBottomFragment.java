package com.example.exam_hmme.home.movie;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.exam_hmme.R;
import com.example.exam_hmme.data.model.MovieDetails;
import com.example.exam_hmme.databinding.BottomDetailsMoviesBinding;
import com.example.exam_hmme.utils.Constantes;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.squareup.picasso.Picasso;

public class DetailsBottomFragment extends BottomSheetDialogFragment {
    public static final String TAG = "ActionBottomDialog";
    private MovieDetails movies;
    private BottomDetailsMoviesBinding bottomBinding;

    public DetailsBottomFragment(Application application) {
    }

    public static DetailsBottomFragment newInstance(Application application){
        return new DetailsBottomFragment(application);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            movies = getArguments().getParcelable("MovieDetails");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        bottomBinding = DataBindingUtil.inflate(inflater, R.layout.bottom_details_movies, container, false);
        return bottomBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Picasso.get().load(Constantes.URL_DOWLAND_IMG + movies.getPoster_path()).into(bottomBinding.moviePoster);
        bottomBinding.title.setText(movies.getOriginal_title());
        bottomBinding.descripcion.setText(movies.getOverview());
        bottomBinding.date.setText(movies.getRelease_date());
        bottomBinding.votes.setText(movies.getVote_count());
    }
}
