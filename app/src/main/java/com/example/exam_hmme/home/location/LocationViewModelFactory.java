package com.example.exam_hmme.home.location;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.exam_hmme.data.repository.LocationRepository;

public class LocationViewModelFactory implements ViewModelProvider.Factory  {

    private Application application;


    public LocationViewModelFactory(Application application) {
        this.application = application;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(LocationViewModel.class)){
            return (T) new LocationViewModel(application, LocationRepository.getInstance());
        }else{
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
