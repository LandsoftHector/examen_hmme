package com.example.exam_hmme.home.location;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.exam_hmme.R;
import com.example.exam_hmme.data.adapter.AdapterLocation;
import com.example.exam_hmme.databinding.FragmentLocationBinding;


public class LocationFragment extends Fragment {
    FragmentLocationBinding locationBinding;
    private LocationViewModel locationViewModel;
    private AdapterLocation adapterLocation;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        locationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_location, container, false);
        locationViewModel = new ViewModelProvider(this, new LocationViewModelFactory(getActivity().getApplication())).get(LocationViewModel.class);
        View root = locationBinding.getRoot();

        locationViewModel.getDatos();
        locationBinding.recyclerViewLocation.setVisibility(View.GONE);
        locationBinding.progressLocation.setVisibility(View.VISIBLE);

        locationViewModel.getMutableLiveData().observe(getViewLifecycleOwner(), usuarios -> {
            if(usuarios== null){
                return;
            }
            if(usuarios!= null){
                adapterLocation = new AdapterLocation(getActivity(),usuarios);
                locationBinding.recyclerViewLocation.setVisibility(View.VISIBLE);
                locationBinding.progressLocation.setVisibility(View.GONE);
                locationBinding.recyclerViewLocation.setAdapter(adapterLocation);
                locationBinding.recyclerViewLocation.setLayoutManager(new LinearLayoutManager(getActivity()));
            }
        });


        return root;
    }
}