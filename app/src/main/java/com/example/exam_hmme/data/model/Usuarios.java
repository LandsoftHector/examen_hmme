package com.example.exam_hmme.data.model;

public class Usuarios {

    private String nombre;
    private String location;

    public Usuarios(String nombre, String location) {
        this.nombre = nombre;
        this.location = location;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
