package com.example.exam_hmme.data.repository;

import androidx.lifecycle.MutableLiveData;

import com.example.exam_hmme.data.model.Usuarios;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class LocationRepository {

    private static volatile LocationRepository instance;
    MutableLiveData<List<Usuarios>> mutableLiveData = new MutableLiveData<>();

    public static LocationRepository getInstance() {
        if (instance == null) {
            instance = new LocationRepository();
        }
        return instance;
    }

    public void getDatosUsuario(){
        try {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            List<Usuarios> usuariosList = new ArrayList<>();
            ListenerRegistration listener = db.collection("Usuarios")
                    .addSnapshotListener((queryDocumentSnapshots, e) -> {
                        if(e != null){
                            return;
                        }
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots){
                            String name = document.get("nombre").toString();
                            String loc = document.get("location").toString();
                            Usuarios usuarios = new Usuarios(name,loc);
                            usuariosList.add(usuarios);
                        }

                        if(usuariosList!=null){
                            mutableLiveData.postValue(usuariosList);
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public MutableLiveData<List<Usuarios>> getMutableLiveData() {
        return mutableLiveData;
    }
}
