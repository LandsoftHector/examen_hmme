package com.example.exam_hmme.data.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.exam_hmme.R;
import com.example.exam_hmme.data.model.Usuarios;
import com.example.exam_hmme.databinding.ItemListLocationBinding;
import com.example.exam_hmme.utils.Constantes;

import java.util.List;

public class AdapterLocation extends RecyclerView.Adapter<AdapterLocation.Location>  {

    private List<Usuarios> usuarios;
    private Activity activity;

    public AdapterLocation(Activity activity, List<Usuarios> usuarios) {
        this.activity = activity;
        this.usuarios = usuarios;
    }


    @NonNull
    @Override
    public AdapterLocation.Location onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemListLocationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_list_location,parent,false);
        return new Location(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterLocation.Location holder, int position) {
        Usuarios usuarios1 = usuarios.get(position);
        holder.locationBinding.setUsu(usuarios1);

        holder.locationBinding.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = Constantes.DIRECCION_GOOGLE_MAPS_DEFAULT + " " + usuarios1.getLocation();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(usuarios==null){
            return 0;
        }else{
            return usuarios.size();
        }
    }

    public class Location extends RecyclerView.ViewHolder {

        private ItemListLocationBinding locationBinding;

        public Location(ItemListLocationBinding locationBinding) {
            super(locationBinding.getRoot());
            this.locationBinding = locationBinding;
            locationBinding.location.setPaintFlags(locationBinding.location.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        }
    }
}
