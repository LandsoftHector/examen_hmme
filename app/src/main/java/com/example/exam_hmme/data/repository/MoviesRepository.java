package com.example.exam_hmme.data.repository;

import androidx.lifecycle.MutableLiveData;

import com.example.exam_hmme.client.network.OpenService;
import com.example.exam_hmme.client.network.RetrofitOpenService;
import com.example.exam_hmme.data.model.Movies;
import com.example.exam_hmme.utils.Constantes;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesRepository {

    private static volatile MoviesRepository instance;
    MutableLiveData<Movies> moviesMutableLiveData = new MutableLiveData<>();

    public static MoviesRepository getInstance() {
        if (instance == null) {
            instance = new MoviesRepository();
        }
        return instance;
    }

    public void getMoviesDiscover(String apiKey) {

        OpenService api = RetrofitOpenService.getInstance(Constantes.URL_SERVER_DISCOVERY).getOpenService();

        HashMap<String, String> options = new HashMap<>();
        options.put("api_key", apiKey);
        options.put("language", "en-US");
        options.put("sort_by", "popularity.desc");

        Call<JsonElement> call = api.getMoviesDiscovery(options);

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if(response.isSuccessful()){
                    Movies movies = new Gson().fromJson(response.body(), new TypeToken<Movies>() {}.getType());
                    if(movies != null){
                        moviesMutableLiveData.postValue(movies);
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }

    public MutableLiveData<Movies> getMoviesMutableLiveData() {
        return moviesMutableLiveData;
    }
}
