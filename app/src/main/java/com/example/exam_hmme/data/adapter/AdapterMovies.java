package com.example.exam_hmme.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.exam_hmme.R;
import com.example.exam_hmme.data.model.MovieDetails;
import com.example.exam_hmme.data.model.Movies;
import com.example.exam_hmme.utils.Constantes;
import com.squareup.picasso.Picasso;

public class AdapterMovies extends RecyclerView.Adapter<AdapterMovies.ViewHolder> {

    private Context context;
    private Movies movies;
    private OnItemPosicionClick onItemPosicionClick;

    public AdapterMovies(Context context, Movies movies, OnItemPosicionClick onItemPosicionClick ) {
        this.context =context;
        this.movies = movies;
        this.onItemPosicionClick = onItemPosicionClick;
    }

    @NonNull
    @Override
    public AdapterMovies.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.item_imagen_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(contactView,onItemPosicionClick);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMovies.ViewHolder holder, int position) {
        MovieDetails movieDetails = movies.getResults().get(position);
        holder.textViewName.setText(movieDetails.getOriginal_title());
        Picasso.get().load(Constantes.URL_DOWLAND_IMG + movieDetails.getPoster_path()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        if(movies == null){
            return 0;
        }else{
            return movies.getResults().size();
        }
    }

    public MovieDetails getposicionmovieDetails(int posicion){
        return movies.getResults().get(posicion);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageView;
        private TextView textViewName;
        private OnItemPosicionClick onItemPosicionClick;
        public ViewHolder(@NonNull View itemView, OnItemPosicionClick onItemPosicionClick) {
            super(itemView);
            imageView = itemView.findViewById(R.id.movie_poster);
            textViewName = itemView.findViewById(R.id.movie_name);
            this.onItemPosicionClick = onItemPosicionClick;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemPosicionClick.onClick(getAdapterPosition());
        }
    }

    public interface OnItemPosicionClick{
        void onClick(int posicion);
    }
}
