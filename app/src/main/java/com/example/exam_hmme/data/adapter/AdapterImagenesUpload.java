package com.example.exam_hmme.data.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.exam_hmme.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterImagenesUpload extends RecyclerView.Adapter<AdapterImagenesUpload.MyViewHolder> {
    Context context;
    List<Uri> mList;

    public AdapterImagenesUpload(Context context, List<Uri> mList) {
        this.context = context;
        this.mList = mList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.item_image_upload, parent, false);
        AdapterImagenesUpload.MyViewHolder viewHolder = new AdapterImagenesUpload.MyViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Uri pathImagen = mList.get(position);
        Picasso.get().load(pathImagen).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        if(mList==null){
            return 0;
        }else{
            return mList.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textViewName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.movie_poster);
            textViewName = itemView.findViewById(R.id.movie_name);
        }
    }
}
