package com.example.exam_hmme.client.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitOpenService {

    private OpenService openService;

    public RetrofitOpenService(String url) {
        buildAISOpenService(url);
    }

    public static RetrofitOpenService getInstance(String url) {
        return new RetrofitOpenService(url);
    }

    public OpenService getOpenService(){
        return openService;
    }

    private void buildAISOpenService(String url) {
        openService = getBuildRetrofit(url).create(OpenService.class);
    }

    private Retrofit getBuildRetrofit (String url){

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").setLenient().create();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url) // paso la URL  a conectar
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        return retrofit;
    }
}
