package com.example.exam_hmme.client.network;

import com.google.gson.JsonElement;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface OpenService {

    @GET("/3/discover/movie")
    Call<JsonElement> getMoviesDiscovery(@QueryMap Map<String, String> options);
}
