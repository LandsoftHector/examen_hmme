package com.example.exam_hmme.utils;

public class Constantes {

    public static final String apiKey = "b16cd0b3e8a9fe0ef620d43be398958b";
    public static final String DIRECCION_GOOGLE_MAPS_DEFAULT = "http://maps.google.com/maps?q=loc:";
    public static final String URL_SERVER_DISCOVERY = "https://api.themoviedb.org";
    public static final String URL_DOWLAND_IMG = "https://image.tmdb.org/t/p/w500";
}
